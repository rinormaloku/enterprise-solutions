package med.ieval.times.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

/**
 * Created by Rinor Maloku.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WarriorRepositoryTest
{
    @Autowired
    private WarriorRepository warriorRepository;

    @Test
    public void dataImportedAndSetInDB()
    {
        assertTrue(warriorRepository.findAll().size() == 3);
    }
}