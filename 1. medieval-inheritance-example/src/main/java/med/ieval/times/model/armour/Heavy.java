package med.ieval.times.model.armour;

/**
 * Created by Rinor Maloku.
 */
public class Heavy implements Armour
{
    public int hp;

    public Heavy(int hp)
    {
        this.hp = hp;
    }

    @Override
    public int getHp()
    {
        return hp;
    }
}
