package med.ieval.times.model;

import med.ieval.times.model.armour.Mount;
import med.ieval.times.model.wapons.Weapon;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rinor Maloku.
 */
@Entity
public class Warrior
{
    public Warrior() {}

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Mount mount;

    @Column(unique = true)
    private String name;

    @ManyToMany(mappedBy = "bearers", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Weapon> weapons = new ArrayList<>();

    private int age;
    private Date date;

    public Warrior(String name, int age, Date date)
    {
        this.name = name;
        this.age = age;
        this.date = date;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Mount getMount()
    {
        return mount;
    }

    public void setMount(Mount mount)
    {
        this.mount = mount;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "Warrior{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", date=" + date +
                '}';
    }

    public void addWeapon(Weapon weapon)
    {
        weapons.add(weapon);
        weapon.getBearers().add(this);
    }

    public void removeWeapon(Weapon weapon)
    {
        weapons.remove(weapon);
        weapon.getBearers().remove(this);
    }
}
