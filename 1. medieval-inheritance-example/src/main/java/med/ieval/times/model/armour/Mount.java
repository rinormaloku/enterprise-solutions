package med.ieval.times.model.armour;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Rinor Maloku.
 */
@Entity
public class Mount
{
    @Id
    @GeneratedValue
    private long id;

    private int defense;

    @Column(unique = true)
    private String type;

    private int year;

    public Mount()
    {
    }

    public Mount(int defense, String type, int year)
    {
        this.defense = defense;
        this.type = type;
        this.year = year;
    }

    public int getDefense()
    {
        return defense;
    }

    public void setDefense(int defense)
    {
        this.defense = defense;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }
}