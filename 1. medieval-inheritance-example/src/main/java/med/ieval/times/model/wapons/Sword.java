package med.ieval.times.model.wapons;

import javax.persistence.Entity;

/**
 * Created by Rinor Maloku.
 */

@Entity
public class Sword extends Weapon
{
    private int length;

    public Sword() {}

    public Sword(int damage, int year, int length)
    {
        super(damage, year);
        this.length = length;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

}