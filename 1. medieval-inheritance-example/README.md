```
                                   +------------------------+             
                                   |        Weapon          |             
                                   |------------------------|             
                                   |-damage: int            |             
                                   |-year: int              |             
                                   +------------------------+             
                                                |                         
                                                |                         
                                                v                         
 +-------------------------+        +------------------------+            
 |        Warrior          |        |         Sword          |            
 |-------------------------|        |------------------------|            
 |- mount: Mount           |        |-length: int            |            
 |- weapons: List<Weapons> |--------|-bearers: List<Warrior> |            
 +-------------------------+        +------------------------+  
```

Warrior has a list of Weapons.
Weapon is an abstract class.
Sword is an implementation.
To persist the inheritance:

1. Add annotation to the class that has the Id, to the parent.

```
@Inheritance(strategy = InheritanceType.JOINED)		
```

2. Extend weapon from sowrd.

Done