package med.ieval.times.repository;

import med.ieval.times.model.Warrior;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.Date;

/**
 * Created by Rinor Maloku.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomValidatorTest
{
    @Autowired
    private WarriorRepository warriorRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Test if you cannot create a warrior whose name doesn't Include Spartan.
     * @throws ConstraintViolationException
     */
    @Test
    public void testCustomValidator() throws ConstraintViolationException
    {
        thrown.expect(ConstraintViolationException.class);
        thrown.expectMessage("Invalid Warrior Name");

        Warrior wr = new Warrior("Ephialtes", 24, new Date());
        warriorRepository.save(wr);
    }
}