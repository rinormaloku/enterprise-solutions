package med.ieval.times.model.armour;

/**
 * Created by Rinor Maloku.
 */
public interface Armour
{
    int getHp();
}
