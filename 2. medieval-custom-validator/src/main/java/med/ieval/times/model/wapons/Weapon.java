package med.ieval.times.model.wapons;

import med.ieval.times.model.Warrior;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinor Maloku.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Weapon
{
    @Id
    @GeneratedValue
    private Long id;

    private int damage;
    private int year;

    @ManyToMany
    private List<Warrior> bearers = new ArrayList<>();

    public Weapon() {}

    public Weapon(int damage, int year)
    {
        this.damage = damage;
        this.year = year;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getDamage()
    {
        return damage;
    }

    public void setDamage(int damage)
    {
        this.damage = damage;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public List<Warrior> getBearers()
    {
        return bearers;
    }

    public void setBearers(List<Warrior> bearers)
    {
        this.bearers = bearers;
    }

    @Override
    public String toString()
    {
        return "Weapon{" +
                "id=" + id +
                ", damage=" + damage +
                ", year=" + year +
                '}';
    }
}
