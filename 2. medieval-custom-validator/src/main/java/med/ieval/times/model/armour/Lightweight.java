package med.ieval.times.model.armour;

/**
 * Created by Rinor Maloku.
 */
public class Lightweight implements Armour
{
    public int hp;

    @Override
    public int getHp()
    {
        return hp;
    }
}
