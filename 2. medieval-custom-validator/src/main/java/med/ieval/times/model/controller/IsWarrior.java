package med.ieval.times.model.controller;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Rinor Maloku.
 */
@Constraint(validatedBy = {NameValidator.class})
@Target({ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface IsWarrior
{
    String message() default "Invalid Warrior Name";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
