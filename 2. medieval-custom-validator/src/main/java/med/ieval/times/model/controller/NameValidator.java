package med.ieval.times.model.controller;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Rinor Maloku.
 */
public class NameValidator implements ConstraintValidator<IsWarrior, String>
{
    @Override
    public void initialize(IsWarrior constraintAnnotation) {}

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context)
    {
        return value.contains("Spartan");
    }
}