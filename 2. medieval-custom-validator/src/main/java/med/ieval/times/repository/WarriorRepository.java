package med.ieval.times.repository;

import med.ieval.times.model.Warrior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Rinor Maloku.
 */

@Repository
public interface WarriorRepository extends JpaRepository<Warrior, Long>
{
}
