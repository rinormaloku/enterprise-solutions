INSERT INTO warrior (age, date, name) VALUES (24, CURRENT_TIME(), 'Rinor');
INSERT INTO weapon (damage, year) VALUES (100, 1997);
INSERT INTO sword (length, id) VALUES (80, 1);
INSERT INTO weapon (damage, year) VALUES (100, 1997);
INSERT INTO sword (length, id) VALUES (12, 2);
INSERT INTO mount (defense, type, year) VALUES (80,  'Wood', 1807);
INSERT INTO weapon_bearers (weapons_id, bearers_id) VALUES (1, 1);
INSERT INTO weapon_bearers (weapons_id, bearers_id) VALUES (2, 1);
INSERT INTO warrior (age, date, name ) VALUES (29, CURRENT_TIME(), 'Bekan');
INSERT INTO weapon (damage, year) VALUES (97, 1980);
INSERT INTO sword (length, id) VALUES (90, 3);
INSERT INTO weapon (damage, year) VALUES (50, 1987);
INSERT INTO sword (length, id) VALUES (17, 4);
INSERT INTO mount (defense, type, year) VALUES (120, 'Iron', 1880);
INSERT INTO weapon_bearers (weapons_id, bearers_id) VALUES (3, 2);
INSERT INTO weapon_bearers (weapons_id, bearers_id) VALUES (4, 2);
INSERT INTO warrior (age, date, name) VALUES (21, CURRENT_TIME(), 'Loka');
INSERT INTO mount (defense, type, year) VALUES (200, 'Tungsten', 2020);